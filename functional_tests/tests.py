from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys
import time
import unittest
from django.contrib.staticfiles.testing import StaticLiveServerTestCase


MAX_WAIT = 10

class NewVisitorTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        # she feels satisfied and went to sleep.
        self.browser.quit()

    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()

        while True:
            try:
                table = self.browser.find_element_by_id('id_list_table')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)


    def test_can_start_a_list_for_one_user(self):

        # Sarah heard a new online to-do app. she opens the home page.
        self.browser.get(self.live_server_url)

        # 1: second to-do item.
        # She notices the 'to-do' word in home page title
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # she is invited to enter to-do item in a text box.
        input_box = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            input_box.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # she type 'buy threads' into the text box.
        input_box.send_keys('Buy threads')

        # when she hit enter the page updates and show the 'buy threads' as a item
        input_box.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy threads')


        # 2: second to-do item.
        # she is invited again to enter to-do item in a text box.
        input_box = self.browser.find_element_by_id('id_new_item')

        # she type 'buy threads' into the text box.
        input_box.send_keys('Buy buttons')

        # when she hit enter the page updates and show the 'buy threads' as a item
        input_box.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Buy threads')
        self.wait_for_row_in_list_table('2: Buy buttons')      

        
        # then she again add an item 'Sew the t-shirt' and page updates and now shows both items in to-do list.
        # self.fail('Finish the test!')


        # she wonders whether the site gonna remember her list items. then site provides her a url and she can see her iems there.


    def test_multiple_users_can_start_lists_at_different_urls(self):
        
        self.browser.get(self.live_server_url)
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy threads')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy threads')

        sarah_list_url = self.browser.current_url
        self.assertRegex(sarah_list_url, '/lists/.+')
        
        ## we'll start new browser session to mke sure that no info of Sarah's is coming through from cookies etc.
        self.browser.quit()
        self.browser = webdriver.Firefox()

        # Francis visits the home page. There is no sign of the Sarah's list
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy threads', page_text)

        # Francis starts a new list by entering a new item. He is less interesting than Sarah.
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy milk')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        # Francis gets his own unique URL.
        francis_list_url = self.browser.current_url
        self.assertRegex(francis_list_url, '/lists/.+')
        self.assertNotEqual(francis_list_url, sarah_list_url)

        # Again, there is no sign of Sarah's list.
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy threads', page_text)
        self.assertIn('Buy milk', page_text)


    def test_layout_and_styling(self):
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta=10
        )

        inputbox.send_keys('testing')
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: testing')
        inputbox = self.browser.find_element_by_id('id_new_item')

        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta=10
        )


